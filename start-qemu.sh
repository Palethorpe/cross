#!/usr/bin/sh -eu

start_aarch64() {
	qemu-system-aarch64 -M virt \
			    -cpu cortex-a57 \
			    -smp 2 \
			    -m 4G \
			    -kernel aarch64/images/Image \
			    -initrd aarch64/images/rootfs.cpio.gz \
			    -append "rootwait console=ttyAMA0 ignore_loglevel" \
			    -display none \
			    -serial mon:stdio \
			    -netdev user,id=eth0 \
			    -device virtio-net-device,netdev=eth0
}

start_ppc64() {
	qemu-system-ppc64 -M pseries \
			  -cpu POWER8 \
			  -m 4G \
			  -kernel ppc64/images/vmlinux \
			  -initrd ppc64/images/rootfs.cpio.gz \
			  -append "console=hvc0 rootwait ignore_loglevel" \
			  -serial mon:stdio \
			  -display none
}

start_s390x() {
	qemu-system-s390x -M s390-ccw-virtio \
			  -cpu max,zpci=on \
			  -smp 2 \
			  -m 4G \
			  -kernel s390x/images/bzImage \
			  -initrd s390x/images/rootfs.cpio.gz \
			  -append "rootwait net.ifnames=0 biosdevname=0 ignore_loglevel" \
			  -display none \
			  -serial mon:stdio \
			  -net nic,model=virtio \
			  -net user
}

arch=$1

case $arch in
	aarch64) start_aarch64 ;;
	ppc64) start_ppc64 ;;
	s390x) start_s390x ;;
esac
